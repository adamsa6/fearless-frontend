import React, { useEffect, useState} from 'react';


function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [presenterName, setPresenterName] = useState('');
    const [presenterEmail, setPresenterEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            //console.log('this is the first fetch data:', data);

            setConferences(data.conferences);
        }
    }

    useEffect( () => {
        fetchData();
    }, []);

    function handlePresenterNameChange(event) {
        const value = event.target.value;
        setPresenterName(value);
    }

    function handlePresenterEmailChange(event) {
        const value = event.target.value;
        setPresenterEmail(value);
    }

    function handleCompanyNameChange(event) {
        const value = event.target.value;
        setCompanyName(value);
    }

    function handleTitleChange(event) {
        const value = event.target.value;
        setTitle(value);
    }

    function handleSynopsisChange(event) {
        const value = event.target.value;
        setSynopsis(value);
    }

    function handleConferenceChange(event) {
        const value = event.target.value;
        setConference(value);
    }

    async function handleSubmit(event) {
        event.preventDefault();

        const data = {};
        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference
        //console.log('This is the data:', data);

        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        //console.log('This is the fetchConfig:', fetchConfig);

        try {
            const response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
                const newPresentation = await response.json();
                console.log(newPresentation);

                setPresenterName('');
                setPresenterEmail('');
                setCompanyName('');
                setTitle('');
                setSynopsis('');
                setConference('');
            } else {
                console.error(`Error: ${response.status} ${response.statusText}`);
            }
        } catch (error) {
            console.error('Fetch error:', error)
        }
    }

    return (
    <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new presentation</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input value={presenterName} onChange={handlePresenterNameChange} placeholder="Presenter name" required type="text" id="presenter_name" name="presenter_name" className="form-control" />
                  <label htmlFor="presenter_name">Presenter Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={presenterEmail} onChange={handlePresenterEmailChange} placeholder="Presenter email" required type="email" id="presenter_email" name="presenter_email" className="form-control" />
                  <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={companyName} onChange={handleCompanyNameChange} placeholder="Company name" type="text" id="company_name" name="company_name" className="form-control" />
                  <label htmlFor="company_name">Company name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={title} onChange={handleTitleChange} placeholder="Title" required type="text" id="title" name="title" className="form-control" />
                  <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="synopsis" className="form-label">Synopsis</label>
                  <textarea value={synopsis} onChange={handleSynopsisChange} required className="form-control" type="text" id="synopsis" name="synopsis" rows="3"></textarea>
                </div>
                <div className="mb-3">
                  <select value={conference} onChange={handleConferenceChange} required id="conference" name="conference" className="form-select">
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                            <option key={conference.id} value={conference.id}>{conference.name}
                            </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    </div>
    )
}


export default PresentationForm
