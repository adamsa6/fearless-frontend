import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path='conferences'>
          <Route path='new' element={<ConferenceForm />} />
        </Route>
        <Route path='/locations/new' element={<LocationForm />} />
        <Route path='/attendees' element={<AttendeesList attendees={props.attendees} />} />
        <Route path='/presentations/new' element={<PresentationForm />} />
        <Route path='/attendees/new' element={<AttendConferenceForm />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App;
