import React from 'react'
import { NavLink } from 'react-router-dom'


function Nav() {
    return (
        <header>
        <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
          <div className="container-fluid">
            <NavLink to='/' className="navbar-brand" href="/">Conference GO!</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink to='/' className="nav-link active" aria-current="page">Home</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to='/locations/new' className="nav-link" id="new-location-link" aria-current="page">New location</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to='/conferences/new' className="nav-link" id="new-conference-link" aria-current="page">New conference</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to='/presentations/new' className="nav-link" id="new-presentation-link" aria-current="page">New presentation</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to='/attendees' className="nav-link" id="attendees-list" aria-current="page">Attendees</NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    );
}

export default Nav;
